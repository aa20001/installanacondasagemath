const fs = require("fs")
const jsdom = require("jsdom");
const path = require("path");
const {TEXT_NODE, COMMENT_NODE} = require("jsdom/lib/jsdom/living/node-type");


function fragment2html(fragment) {
    let html = "";
    for (const fragmentElement of fragment.childNodes) {
        // console.log(fragmentElement)
        // console.log(fragmentElement.outerHTML, fragmentElement.textContent);
        if (fragmentElement.nodeType === TEXT_NODE) {
            html += fragmentElement.textContent;
        } else if (fragmentElement.nodeType === COMMENT_NODE) {
            html += `<!-- ${fragmentElement.data} -->`;
        } else {
            if (!fragmentElement.outerHTML) {
                console.log(fragmentElement);
            }
            html += fragmentElement.outerHTML;
        }
    }
    return html;
}

function resolvePath(rootPath, filePath, targetPath) {
    if (targetPath.startsWith("/")) {
        return path.join(rootPath, targetPath);
    } else if (targetPath.startsWith("../")) {
        return path.join(path.dirname(path.dirname(filePath)), targetPath);
    } else {
        return path.join(path.dirname(filePath), targetPath);
    }
}

function compile(rootPath, filePath, element) {
    for (const externalElement of element.querySelectorAll("[data-external-replace],[data-external]")) {
        const externalPath = resolvePath(rootPath, filePath, externalElement.dataset.externalReplace || externalElement.dataset.external);
        const html = compileExternal(rootPath, externalPath);
        if (externalElement.dataset.externalReplace) {
            externalElement.outerHTML = html;
        } else {
            externalElement.innerHTML = html;
            // delete externalElement.dataset.external;
        }

    }
}

function compileExternal(rootPath, filePath) {
    const html = fs.readFileSync(filePath, {encoding: "utf-8"});
    const dom = jsdom.JSDOM.fragment(html);
    compile(rootPath, filePath, dom)
    return fragment2html(dom);
}
function compileDocument(rootPath, filePath) {
    const html = fs.readFileSync(filePath, {encoding: "utf-8"});
    const dom = new jsdom.JSDOM(html);
    compile(rootPath, filePath, dom.window.document);
    // const dom = jsdom.JSDOM.fragment(`こんにちは`);
    // console.log(dom.childNodes[0]);
    // console.dir(dom.childNodes[0]);
    // fragment2html(dom);
    return dom.serialize()
}


function main() {
    fs.writeFileSync(path.resolve("./", "index.html"), compileDocument(path.resolve("./"), "index.html"), {encoding: "utf-8"})
}

main();