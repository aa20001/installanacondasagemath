#!/usr/bin/env bash

## 関数定義エリア
function doBuild() {
  target=$1
  isBranch=$2
  
  if [ "$isBranch" != "" ]; then
      git checkout "refs/tags/$target"
  else
      git checkout "$target"
  fi

  export DEST=/tmp/public/${target}
  mkdir "$DEST"
  cp -R ./* "$DEST"

  if [ -f ENV ]; then source ENV; fi

  if [ -f "$DEST/buildScripts/main.sh" ]; then
      "$DEST/buildScripts/main.sh"
  fi

  find "$DEST" -type f -name '*.html' -print0 | xargs -0 sed -i \
    -e "s/{{version}}/${target}/g" \
    -e "s/{{SAGE_VERSION}}/${SAGE_VERSION}/g" \
    -e "s/教員等に相談/質問広場の投稿を確認後質問広場に投稿/g"

}

## 事前準備
# 出力先ディレクトリ作成
mkdir /tmp/public
# リモート情報取得
git fetch --all && git fetch --all --tags
# マスターブランチに切り替え
git checkout -f master && git pull -a
# 最終バージョンの取得
latestTag=$(git describe --tags --abbrev=0)
branches=(
    "master_withIf"
    "master_withoutIf"
    "develop_withIf"
    "develop_withoutIf"
)

## ビルド実行
# 最近のタグに対してビルドを実行
for tag in $(git tag --sort creatordate | tail -n10); do
  doBuild "$tag"
done

# ビルド対象のブランチに対してビルドを実行
for branch in "${branches[@]}"; do
  doBuild "$branch"
done

## 事後処理
# 現在のディレクトリ内容を削除
rm -rf ./*
# リダイレクトファイル準備
cat > /tmp/public/index.html <<- EOM
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="0;URL=./$latestTag">
    <title>Redirecting Latest Version</title></head>
<body><h1>リダイレクト</h1>
<p>最新のバージョンにリダイレクトしています<br>リダイレクトしない場合は、以下のURLをクリックしてください。</p>
<p><a href="./$latestTag">最新版</a></p></body>
</html>
EOM
# 出力ディレクトリを所定の位置に移動
mv /tmp/public ./