# RevealIf
## Usage
### 変数名と質問、選択肢の定義
これらはReveal.jsの初期化部に記述します。  
各変数はこの選択肢の値しか取らず、質問文がない場合は変数名を質問文として扱います。  
```javascript
Reveal.initialize({
    If: {
        variables: {
            os: {type: "radio", ask: "使用しているOSを選択してください", options: [
                {value: "windows10", display: "Windows10"},
                {value: "windows11", display: "Windows11"},
                {value: "unix", display: "unix系(macOS等)"},
            ]},
            terminalType: {type: "select", ask: "スタートを右クリックした時に以下のどちらが表示されましたか？", options: [
                {value: "powershell", display: "PowerShell"},
                {value: "windowsTerminal", display: "ターミナルまたはTerminal"},
            ]},
        },
        shortcuts: {
            isWindows: {
                "type": "operation",
                    "operation": "||",
                    "values": [
                    {"type": "simple", "conditions": {"os": "windows10"}},
                    {"type": "simple", "conditions": {"os": "windows11"}}
                ]
            }
        }
    },
    plugins: [If],
});
```
例えば
```js
os: {type: "radio", ask: "使用しているOSを選択してください", options: [
    {value: "windows10", display: "Windows10"},
    {value: "windows11", display: "Windows11"},
    {value: "unix", display: "unix系(macOS等)"},
]}
```
はPythonで次のコードのような意味を持ちます  
```python
import enum

class OS(enum.Enum):
    windows10 = auto()
    windows11 = auto()
    unix = auto()

os: OS = None

def askOS():
    while res := input("使用しているOSを選択してください(windows10/windows11/unix)>"):
        try:
            res = OS(res)
            break
        except:
            continue
    os = res
    
```
すなわちこのコードの意味は使用する変数名の定義と変数の取りうる値を定義しています。  
又同時に選択肢を表示する際の質問文も定義しています  

また  
```js
isWindows: {
    "type": "operation",
        "operation": "||",
        "values": [
        {"type": "simple", "conditions": {"os": "windows10"}},
        {"type": "simple", "conditions": {"os": "windows11"}}
    ]
}
```
は次のPythonコードと同じ意味を持ちます  
```python
def isWindows():
    return any([os == OS.windows10, os==OS.windows11])
```

### 質問と選択肢の表示による変数の値の設定
```html
<section>
    <div data-if-set="os"></div>
</section>
```
これはPythonにおいて以下のコードと似た意味を持ちます  
```python
askOS()
```

### 条件分岐によるスライドの表示非表示の方法 
`data-if`が要素の属性にあり、子要素に属性`data-if-condition`を含む要素が含まれている時変数による表示非表示の分岐を行います   
条件は要素内の最初のものが使用されます。  
使用する変数が定義されていない場合自動的に設定を促します。
```html
<section data-if>
    <textarea data-if-conditions>
        {"type": "simple", "conditions": {"os": "windows10"}}
    </textarea>
    windows10
</section>
<section data-if>
    <textarea data-if-conditions>
        {"type": "shortcut", "name": "isWindows"}
    </textarea>
    any Windows
</section>
```
`data-if-condition`内の要素はJSON形式で記述した論理式です  

例えば
```html
<section data-if>
    <textarea data-if-conditions>
        {"type": "simple", "conditions": {"os": "windows10"}}
    </textarea>
    windows10
</section>
```
このコードは次のPythonコードと同じような意味を持ちます  
```python
if os is None:
    askOS()

if os == OS.windows10:
    print("windows10")

```

また、
```html
<section data-if>
    <textarea data-if-conditions>
        {"type": "shortcut", "name": "isWindows"}
    </textarea>
    any Windows
</section>
```
このコードは次のPythonコードと同じような意味を持ちます
```python
if os is None:
    askOS()

if isWindows():
    print("any Windows")

```

### 条件分岐に使用できる型
list of types  
* `{type: "simple", conditions: {os: "windows", version: 11}}`  
  conditions内のキーの変数とその値と値がすべて等しいときにTrueになります  
  下記コードと等価  
  ```python
  conditions = {"os": "windows", "version": "11"}
  all([globals()[k] == v for k, v in conditions.items()])
  ```
* `{type; "value", value: value}`  
  valueとして解釈されます。  
  下記コードと等価  
  ```python
  value
  ```
* `{type; "variable", variable: "variable"}`  
  変数variableの値として解釈されます。  
  下記コードと等価  
  ```python
  globals()["variable"]
  ```
* `{type: "operation", ...}`  
  下に記すが内容についてはなんとなく理解できるはず  

list of operations  
`{type: "operation"", operation: "===", left: {type: "variable", variable: "os"}, right: {type: "value", value: "windows"}}`  
`{type: "operation", operation: "!==", left: {type: "variable", variable: "os"}, right: {type: "value", value: "windows"}}`  
`{type: "operation", operation: "&&", values: [...values]}`  
`{type: "operation", operation: "||", values: [...values]}`  
`{type: "operation", operation: "!", value: {type: "operation", operation: "===", left: {type: "variable", variable: "os"}, right: {type: "value", value: "windows"}}}`  

